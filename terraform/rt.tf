resource "aws_route_table" "rtb-ba6bd9c4" {
    vpc_id     = "vpc-44d2e53e"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "igw-0daa3076"
    }

    tags {
    }
}

resource "aws_route_table" "rtb-0b1536c38425b09ef" {
    vpc_id     = "vpc-046a2dcfc1ed27038"

    route {
        cidr_block = "169.254.32.60/30"
        gateway_id = "igw-0b50027dc7b63194f"
    }

    route {
        cidr_block = "169.254.222.196/30"
        gateway_id = "igw-0b50027dc7b63194f"
    }

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "igw-0b50027dc7b63194f"
    }

    propagating_vgws = ["vgw-0bdfc16831878dd69"]

    tags {
    }
}

