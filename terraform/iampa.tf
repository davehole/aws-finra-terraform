resource "aws_iam_policy_attachment" "SupportUser-policy-attachment" {
    name       = "SupportUser-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/job-function/SupportUser"
    groups     = []
    users      = ["chris.powell@inforalgo.com"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AmazonEC2FullAccess-policy-attachment" {
    name       = "AmazonEC2FullAccess-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
    groups     = []
    users      = []
    roles      = ["FinraAdmin"]
}

resource "aws_iam_policy_attachment" "Billing-policy-attachment" {
    name       = "Billing-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/job-function/Billing"
    groups     = []
    users      = ["tmilsom@astuta.com", "michael.vas@inforalgo.com"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AWSOrganizationsServiceTrustPolicy-policy-attachment" {
    name       = "AWSOrganizationsServiceTrustPolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSOrganizationsServiceTrustPolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForOrganizations"]
}

resource "aws_iam_policy_attachment" "IAMUserChangePassword-policy-attachment" {
    name       = "IAMUserChangePassword-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/IAMUserChangePassword"
    groups     = []
    users      = ["chris.powell@inforalgo.com", "tmilsom@astuta.com", "michael.vas@inforalgo.com"]
    roles      = []
}

resource "aws_iam_policy_attachment" "AWSSupportServiceRolePolicy-policy-attachment" {
    name       = "AWSSupportServiceRolePolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSSupportServiceRolePolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForSupport"]
}

resource "aws_iam_policy_attachment" "AWSTrustedAdvisorServiceRolePolicy-policy-attachment" {
    name       = "AWSTrustedAdvisorServiceRolePolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSTrustedAdvisorServiceRolePolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForTrustedAdvisor"]
}

resource "aws_iam_policy_attachment" "AWSVPCS2SVpnServiceRolePolicy-policy-attachment" {
    name       = "AWSVPCS2SVpnServiceRolePolicy-policy-attachment"
    policy_arn = "arn:aws:iam::aws:policy/aws-service-role/AWSVPCS2SVpnServiceRolePolicy"
    groups     = []
    users      = []
    roles      = ["AWSServiceRoleForVPCS2SVPN"]
}

