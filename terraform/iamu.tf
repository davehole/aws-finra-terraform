resource "aws_iam_user" "chris-powell-inforalgo-com" {
    name = "chris.powell@inforalgo.com"
    path = "/"
}

resource "aws_iam_user" "michael-vas-inforalgo-com" {
    name = "michael.vas@inforalgo.com"
    path = "/"
}

resource "aws_iam_user" "tmilsom-astuta-com" {
    name = "tmilsom@astuta.com"
    path = "/"
}

