resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-MIRROR-CT-Privatelink-CCRP-rClientVpceSecurityGroup-10EDP772R1APR" {
    name        = "CAT-MIRROR-CT-Privatelink-CCRP-rClientVpceSecurityGroup-10EDP772R1APR"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "CAT" = "CatPrivatelink"
        "aws:cloudformation:stack-name" = "CAT-MIRROR-CT-Privatelink-CCRP"
        "Name" = "ccrp-rClientVpceSecurityGroup"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-MIRROR-CT-Privatelink-CCRP/ae0ad880-6e1f-11eb-a79f-0e211a4ad657"
        "ServiceType" = "ccrp"
        "Env" = "CT"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-PROD-Privatelink-CAUTH-rClientVpceSecurityGroup-SY71VNVNLW7L" {
    name        = "CAT-PROD-Privatelink-CAUTH-rClientVpceSecurityGroup-SY71VNVNLW7L"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "Name" = "cauth-rClientVpceSecurityGroup"
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "CAT" = "CatPrivatelink"
        "ServiceType" = "cauth"
        "Env" = "Prod"
        "aws:cloudformation:stack-name" = "CAT-PROD-Privatelink-CAUTH"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-PROD-Privatelink-CAUTH/c7e24ff0-7a63-11ea-a08a-0a1f71f25d05"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-PROD-Privatelink-CCRP-rClientVpceSecurityGroup-1J8MH7N8H8ZOL" {
    name        = "CAT-PROD-Privatelink-CCRP-rClientVpceSecurityGroup-1J8MH7N8H8ZOL"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "CAT" = "CatPrivatelink"
        "Env" = "Prod"
        "Name" = "ccrp-rClientVpceSecurityGroup"
        "aws:cloudformation:stack-name" = "CAT-PROD-Privatelink-CCRP"
        "ServiceType" = "ccrp"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-PROD-Privatelink-CCRP/2469b5c0-7a63-11ea-a42a-0ed2247e0ea9"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-PrivateLink-CCFT-rClientVpceSecurityGroup-1FZMH3VTIZ9U" {
    name        = "CAT-PrivateLink-CCFT-rClientVpceSecurityGroup-1FZMH3VTIZ9U"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "ServiceType" = "ccft"
        "Env" = "CT"
        "Name" = "ccft-rClientVpceSecurityGroup"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-PrivateLink-CCFT/e2bbbb80-4cbf-11ea-a899-0a0faac5843d"
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "aws:cloudformation:stack-name" = "CAT-PrivateLink-CCFT"
        "CAT" = "CatPrivatelink"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-PROD-Privatelink-CCFT-rClientVpceSecurityGroup-1TG340PME6X7I" {
    name        = "CAT-PROD-Privatelink-CCFT-rClientVpceSecurityGroup-1TG340PME6X7I"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "Name" = "ccft-rClientVpceSecurityGroup"
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "CAT" = "CatPrivatelink"
        "Env" = "Prod"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-PROD-Privatelink-CCFT/6ba57290-7a62-11ea-ba00-0ea47a628cf7"
        "ServiceType" = "ccft"
        "aws:cloudformation:stack-name" = "CAT-PROD-Privatelink-CCFT"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-MIRROR-CT-Privatelink-CCFT-rClientVpceSecurityGroup-JF0D7SDQC3V0" {
    name        = "CAT-MIRROR-CT-Privatelink-CCFT-rClientVpceSecurityGroup-JF0D7SDQC3V0"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "Env" = "CT"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-MIRROR-CT-Privatelink-CCFT/2eefe500-6e1e-11eb-83fd-12b6a099270b"
        "ServiceType" = "ccft"
        "Name" = "ccft-rClientVpceSecurityGroup"
        "aws:cloudformation:stack-name" = "CAT-MIRROR-CT-Privatelink-CCFT"
        "CAT" = "CatPrivatelink"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-launch-wizard-1" {
    name        = "launch-wizard-1"
    description = "launch-wizard-1 created 2020-05-01T09:46:02.942+01:00"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 636
        to_port         = 636
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 389
        to_port         = 389
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port       = 18080
        to_port         = 18080
        protocol        = "tcp"
        cidr_blocks     = ["185.119.235.64/32", "62.6.191.194/32"]
    }


    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-launch-wizard-3" {
    name        = "launch-wizard-3"
    description = "launch-wizard-3 created 2020-02-11T12:46:09.002+00:00"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 989
        to_port         = 989
        protocol        = "tcp"
        cidr_blocks     = ["62.6.191.194/32", "185.119.235.64/32"]
    }

    ingress {
        from_port       = 1001
        to_port         = 1001
        protocol        = "tcp"
        cidr_blocks     = ["62.6.191.194/32", "185.119.235.64/32"]
    }

    ingress {
        from_port       = 999
        to_port         = 999
        protocol        = "tcp"
        cidr_blocks     = ["62.6.191.194/32", "185.119.235.64/32"]
    }

    ingress {
        from_port       = 1000
        to_port         = 1000
        protocol        = "tcp"
        cidr_blocks     = ["62.6.191.194/32", "185.119.235.64/32"]
    }

    ingress {
        from_port       = 3389
        to_port         = 3389
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-PrivateLink-CAUTH-rClientVpceSecurityGroup-15OFM2OIOUICG" {
    name        = "CAT-PrivateLink-CAUTH-rClientVpceSecurityGroup-15OFM2OIOUICG"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-PrivateLink-CAUTH/b9dade00-4cbd-11ea-bf44-0ec446493c7d"
        "Env" = "CT"
        "ServiceType" = "cauth"
        "Name" = "cauth-rClientVpceSecurityGroup"
        "CAT" = "CatPrivatelink"
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "aws:cloudformation:stack-name" = "CAT-PrivateLink-CAUTH"
    }
}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-default" {
    name        = "default"
    description = "default VPC security group"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
        ipv6_cidr_blocks     = ["::/0"]
    }


    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

resource "aws_security_group" "vpc-046a2dcfc1ed27038-CAT-PrivateLink-CCRP-rClientVpceSecurityGroup-1QXSAOQ5MU2FM" {
    name        = "CAT-PrivateLink-CCRP-rClientVpceSecurityGroup-1QXSAOQ5MU2FM"
    description = "CAT-SecurityGroupForPrivateLink"
    vpc_id      = "vpc-046a2dcfc1ed27038"

    ingress {
        from_port       = 443
        to_port         = 443
        protocol        = "tcp"
        cidr_blocks     = ["0.0.0.0/0"]
    }


    egress {
        from_port       = -1
        to_port         = -1
        protocol        = "icmp"
        cidr_blocks     = ["127.0.0.1/32"]
    }

    tags {
        "ServiceType" = "ccrp"
        "aws:cloudformation:stack-id" = "arn:aws:cloudformation:us-east-1:245847763953:stack/CAT-PrivateLink-CCRP/7e1c69f0-4cbe-11ea-95a4-0a545e22b5be"
        "CAT" = "CatPrivatelink"
        "aws:cloudformation:logical-id" = "rClientVpceSecurityGroup"
        "aws:cloudformation:stack-name" = "CAT-PrivateLink-CCRP"
        "Name" = "ccrp-rClientVpceSecurityGroup"
        "Env" = "CT"
    }
}

resource "aws_security_group" "vpc-44d2e53e-default" {
    name        = "default"
    description = "default VPC security group"
    vpc_id      = "vpc-44d2e53e"

    ingress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        security_groups = []
        self            = true
    }


    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

}

