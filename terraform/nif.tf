resource "aws_network_interface" "eni-04f413bd12a6d61c4" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.26.123"]
    security_groups   = ["sg-0b70018a48090da84"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0197a7048d47e930c" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.19.162"]
    security_groups   = ["sg-04230deff6d634d8c"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0223cd91ab83ed7d4" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.20.143"]
    security_groups   = ["sg-0248806d9cbf966e3"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-09f7068d36e3ae9fe" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.16.31"]
    security_groups   = ["sg-04350de30f1cab46d"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-028d0b7687ab489c0" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.24.214"]
    security_groups   = ["sg-002492de8add3e08d"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0085ef97aeaf5db9f" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.18.150"]
    security_groups   = ["sg-06936e910e366b69e"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0b456a6b3db96c634" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.27.49"]
    security_groups   = ["sg-01332e2a750a8538a"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-06fa0bd7fcfb38266" {
    subnet_id         = "subnet-01d926c4a197736d8"
    private_ips       = ["10.1.19.207"]
    security_groups   = ["sg-0fcad410046455298"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-03796e55cf8cbc3f5" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.70.128"]
    security_groups   = ["sg-04350de30f1cab46d"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-07acff5fc51e59fbb" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.76.222"]
    security_groups   = ["sg-01332e2a750a8538a"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0e2c8c9690bc55011" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.68.111"]
    security_groups   = ["sg-0b70018a48090da84"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0126b3440b1f3be6a" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.70.155"]
    security_groups   = ["sg-002492de8add3e08d"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-03f82cc0dd9f2e04a" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.76.167"]
    security_groups   = ["sg-06936e910e366b69e"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-008d67feb61d6c68d" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.76.98"]
    security_groups   = ["sg-0fcad410046455298"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-04a80eebde2c26709" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.65.160"]
    security_groups   = ["sg-0248806d9cbf966e3"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-085e648989dc18848" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.77.246"]
    security_groups   = ["sg-04230deff6d634d8c"]
    source_dest_check = true
}

resource "aws_network_interface" "eni-0d8d6ce55ca1027b0" {
    subnet_id         = "subnet-04a3c7319981aea38"
    private_ips       = ["10.1.69.152"]
    security_groups   = ["sg-0adb8ed88bb0246ca"]
    source_dest_check = true
    attachment {
        instance     = "i-03db670c49515b3e8"
        device_index = 0
    }
}

