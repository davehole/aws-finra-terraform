resource "aws_route53_record" "-NS" {
    zone_id = "Z0184941S4MTRXCMHFBJ"
    name    = ""
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "-SOA" {
    zone_id = "Z0184941S4MTRXCMHFBJ"
    name    = ""
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "ewslogin-ct-fip-catnms-com-A" {
    zone_id = "Z02243722J3V5V437J184"
    name    = "ewslogin-ct.fip.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-0daeb8cd1830c42d4-k9y0de6x.vpce-svc-03ba9b8699226d3bb.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ewslogin-ct-fip-catnms-com-NS" {
    zone_id = "Z02243722J3V5V437J184"
    name    = "ewslogin-ct.fip.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "ewslogin-ct-fip-catnms-com-SOA" {
    zone_id = "Z02243722J3V5V437J184"
    name    = "ewslogin-ct.fip.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "ews-ct-fip-catnms-com-A" {
    zone_id = "Z022403310QSAWHZCP5TT"
    name    = "ews-ct.fip.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-0daeb8cd1830c42d4-k9y0de6x.vpce-svc-03ba9b8699226d3bb.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ews-ct-fip-catnms-com-NS" {
    zone_id = "Z022403310QSAWHZCP5TT"
    name    = "ews-ct.fip.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "ews-ct-fip-catnms-com-SOA" {
    zone_id = "Z022403310QSAWHZCP5TT"
    name    = "ews-ct.fip.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "reporterportal-pl-ct-catnms-com-A" {
    zone_id = "Z02228531ALSKG8M5JRIO"
    name    = "reporterportal-pl.ct.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-06a25051c9487c5ea-duuaubjm.vpce-svc-07cb86be59e369191.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "reporterportal-pl-ct-catnms-com-NS" {
    zone_id = "Z02228531ALSKG8M5JRIO"
    name    = "reporterportal-pl.ct.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "reporterportal-pl-ct-catnms-com-SOA" {
    zone_id = "Z02228531ALSKG8M5JRIO"
    name    = "reporterportal-pl.ct.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "sftp-pl-ct-catnms-com-A" {
    zone_id = "Z02216377Y5X9TP8KLQV"
    name    = "sftp-pl.ct.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-035df30d8b8229dec-rxq6iub4.vpce-svc-0424a04ad088c97dc.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "sftp-pl-ct-catnms-com-NS" {
    zone_id = "Z02216377Y5X9TP8KLQV"
    name    = "sftp-pl.ct.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "sftp-pl-ct-catnms-com-SOA" {
    zone_id = "Z02216377Y5X9TP8KLQV"
    name    = "sftp-pl.ct.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "cauth-catnms-com-A" {
    zone_id = "Z0227179BTSAL0ZMR7K4"
    name    = "cauth.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-0daeb8cd1830c42d4-k9y0de6x.vpce-svc-03ba9b8699226d3bb.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "cauth-catnms-com-NS" {
    zone_id = "Z0227179BTSAL0ZMR7K4"
    name    = "cauth.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "cauth-catnms-com-SOA" {
    zone_id = "Z0227179BTSAL0ZMR7K4"
    name    = "cauth.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "ccrp-catnms-com-A" {
    zone_id = "Z02165193QALKFD5AXHVU"
    name    = "ccrp.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-031f0d1ca4794f43e-h3fofsti.vpce-svc-01b40566d0d9f1eab.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ccrp-catnms-com-NS" {
    zone_id = "Z02165193QALKFD5AXHVU"
    name    = "ccrp.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "ccrp-catnms-com-SOA" {
    zone_id = "Z02165193QALKFD5AXHVU"
    name    = "ccrp.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "ccft-catnms-com-A" {
    zone_id = "Z02159482D65P287ZV6CR"
    name    = "ccft.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-08b4bb1f3c1998010-anam2pdm.vpce-svc-073af1622eb1c9c76.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ccft-catnms-com-NS" {
    zone_id = "Z02159482D65P287ZV6CR"
    name    = "ccft.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "ccft-catnms-com-SOA" {
    zone_id = "Z02159482D65P287ZV6CR"
    name    = "ccft.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "sftp-pl-catnms-com-NS" {
    zone_id = "Z03784311MLUBKGDEP2ZE"
    name    = "sftp-pl.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "sftp-pl-catnms-com-SOA" {
    zone_id = "Z03784311MLUBKGDEP2ZE"
    name    = "sftp-pl.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "reporterportal-pl-catnms-com-NS" {
    zone_id = "Z037789930P8RVODB9LJW"
    name    = "reporterportal-pl.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "reporterportal-pl-catnms-com-SOA" {
    zone_id = "Z037789930P8RVODB9LJW"
    name    = "reporterportal-pl.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "ews-fip-catnms-com-A" {
    zone_id = "Z0312170VUJI5YLC8LD1"
    name    = "ews.fip.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-0239efda14621eeb1-ktr29gld.vpce-svc-095f952c22a544511.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ews-fip-catnms-com-NS" {
    zone_id = "Z0312170VUJI5YLC8LD1"
    name    = "ews.fip.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "ews-fip-catnms-com-SOA" {
    zone_id = "Z0312170VUJI5YLC8LD1"
    name    = "ews.fip.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

resource "aws_route53_record" "ewslogin-fip-catnms-com-A" {
    zone_id = "Z029614334PFFWU9NO38T"
    name    = "ewslogin.fip.catnms.com"
    type    = "A"

    alias {
        name    = "vpce-0239efda14621eeb1-ktr29gld.vpce-svc-095f952c22a544511.us-east-1.vpce.amazonaws.com"
        zone_id = "Z7HUB22UULQXV"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ewslogin-fip-catnms-com-NS" {
    zone_id = "Z029614334PFFWU9NO38T"
    name    = "ewslogin.fip.catnms.com"
    type    = "NS"
    records = ["ns-1536.awsdns-00.co.uk.", "ns-0.awsdns-00.com.", "ns-1024.awsdns-00.org.", "ns-512.awsdns-00.net."]
    ttl     = "172800"

}

resource "aws_route53_record" "ewslogin-fip-catnms-com-SOA" {
    zone_id = "Z029614334PFFWU9NO38T"
    name    = "ewslogin.fip.catnms.com"
    type    = "SOA"
    records = ["ns-1536.awsdns-00.co.uk. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400"]
    ttl     = "900"

}

