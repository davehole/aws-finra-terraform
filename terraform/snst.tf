resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCFT-Inforalgo-ccft" {
  name            = "CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCFT-Inforalgo-ccft"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCFT-Inforalgo-ccft"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCFT-Inforalgo-ccft"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCRP-Inforalgo-ccrp" {
  name            = "CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCRP-Inforalgo-ccrp"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCRP-Inforalgo-ccrp"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-MIRROR-CT-Privatelink-CCRP-Inforalgo-ccrp"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CAUTH-Inforalgo-cauth" {
  name            = "CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CAUTH-Inforalgo-cauth"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CAUTH-Inforalgo-cauth"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CAUTH-Inforalgo-cauth"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCFT-Infiralgo-ccft" {
  name            = "CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCFT-Infiralgo-ccft"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCFT-Infiralgo-ccft"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCFT-Infiralgo-ccft"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCRP-Inforalgo-ccrp" {
  name            = "CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCRP-Inforalgo-ccrp"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCRP-Inforalgo-ccrp"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PROD-Privatelink-CCRP-Inforalgo-ccrp"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-PrivateLink-CAUTH-Infiralgo-cauth" {
  name            = "CAT-PrivateLink-245847763953-CAT-PrivateLink-CAUTH-Infiralgo-cauth"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PrivateLink-CAUTH-Infiralgo-cauth"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PrivateLink-CAUTH-Infiralgo-cauth"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-PrivateLink-CCFT-Inforalgo-ccft" {
  name            = "CAT-PrivateLink-245847763953-CAT-PrivateLink-CCFT-Inforalgo-ccft"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PrivateLink-CCFT-Inforalgo-ccft"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PrivateLink-CCFT-Inforalgo-ccft"
    }
  ]
}
POLICY
}

resource "aws_sns_topic" "CAT-PrivateLink-245847763953-CAT-PrivateLink-CCRP-Inforalgo-ccrp" {
  name            = "CAT-PrivateLink-245847763953-CAT-PrivateLink-CCRP-Inforalgo-ccrp"
  display_name    = ""
  policy          = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "CatPrivatelink",
  "Statement": [
    {
      "Sid": "AllowAccountPrincipalsAccessIfIamPermits",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::245847763953:root"
      },
      "Action": [
        "SNS:Publish",
        "SNS:RemovePermission",
        "SNS:SetTopicAttributes",
        "SNS:DeleteTopic",
        "SNS:ListSubscriptionsByTopic",
        "SNS:GetTopicAttributes",
        "SNS:Receive",
        "SNS:AddPermission",
        "SNS:Subscribe"
      ],
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PrivateLink-CCRP-Inforalgo-ccrp"
    },
    {
      "Sid": "AllowEndpointServiceToPublishNotifications",
      "Effect": "Allow",
      "Principal": {
        "Service": "vpce.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "arn:aws:sns:us-east-1:245847763953:CAT-PrivateLink-245847763953-CAT-PrivateLink-CCRP-Inforalgo-ccrp"
    }
  ]
}
POLICY
}

