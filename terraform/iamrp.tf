resource "aws_iam_role_policy" "CAT-MIRROR-CT-Privatelink-CCFT-rLambdaRole-82BGT4M8529G_CAT-LambdaRolePolicy-us-east-1-CAT-MIRROR-CT-Privatelink-CCFT" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-MIRROR-CT-Privatelink-CCFT"
    role   = "CAT-MIRROR-CT-Privatelink-CCFT-rLambdaRole-82BGT4M8529G"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-MIRROR-CT-Privatelink-CCRP-rLambdaRole-LX19GC6UXWER_CAT-LambdaRolePolicy-us-east-1-CAT-MIRROR-CT-Privatelink-CCRP" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-MIRROR-CT-Privatelink-CCRP"
    role   = "CAT-MIRROR-CT-Privatelink-CCRP-rLambdaRole-LX19GC6UXWER"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-PrivateLink-CAUTH-rLambdaRole-KL2A91XSNV4N_CAT-LambdaRolePolicy-us-east-1-CAT-PrivateLink-CAUTH" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-PrivateLink-CAUTH"
    role   = "CAT-PrivateLink-CAUTH-rLambdaRole-KL2A91XSNV4N"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-PrivateLink-CCFT-rLambdaRole-1I2YCLSU5NY4J_CAT-LambdaRolePolicy-us-east-1-CAT-PrivateLink-CCFT" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-PrivateLink-CCFT"
    role   = "CAT-PrivateLink-CCFT-rLambdaRole-1I2YCLSU5NY4J"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-PrivateLink-CCRP-rLambdaRole-1AC1UNXA5AVTW_CAT-LambdaRolePolicy-us-east-1-CAT-PrivateLink-CCRP" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-PrivateLink-CCRP"
    role   = "CAT-PrivateLink-CCRP-rLambdaRole-1AC1UNXA5AVTW"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-PROD-Privatelink-CAUTH-rLambdaRole-1HEOHLLNTXQ6Q_CAT-LambdaRolePolicy-us-east-1-CAT-PROD-Privatelink-CAUTH" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-PROD-Privatelink-CAUTH"
    role   = "CAT-PROD-Privatelink-CAUTH-rLambdaRole-1HEOHLLNTXQ6Q"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-PROD-Privatelink-CCFT-rLambdaRole-RPI61UBZ5H69_CAT-LambdaRolePolicy-us-east-1-CAT-PROD-Privatelink-CCFT" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-PROD-Privatelink-CCFT"
    role   = "CAT-PROD-Privatelink-CCFT-rLambdaRole-RPI61UBZ5H69"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy" "CAT-PROD-Privatelink-CCRP-rLambdaRole-9XP4RTTB5T3C_CAT-LambdaRolePolicy-us-east-1-CAT-PROD-Privatelink-CCRP" {
    name   = "CAT-LambdaRolePolicy-us-east-1-CAT-PROD-Privatelink-CCRP"
    role   = "CAT-PROD-Privatelink-CCRP-rLambdaRole-9XP4RTTB5T3C"
    policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "route53:CreateHostedZone",
        "route53:ListHostedZonesByName",
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets",
        "ec2:DescribeVpcs",
        "ec2:DescribeRegions"
      ],
      "Resource": "*",
      "Effect": "Allow",
      "Sid": "DNS"
    },
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*:log-stream:*",
      "Effect": "Allow",
      "Sid": "Logstream"
    },
    {
      "Action": [
        "logs:PutLogEvents",
        "logs:CreateLogGroup"
      ],
      "Resource": "arn:aws:logs:*:*:log-group:/aws/lambda/*rLambdaPrivateHostedZone*",
      "Effect": "Allow",
      "Sid": "Loggroup"
    }
  ]
}
POLICY
}

