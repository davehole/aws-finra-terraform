resource "aws_iam_role" "AWSServiceRoleForOrganizations" {
    name               = "AWSServiceRoleForOrganizations"
    path               = "/aws-service-role/organizations.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "organizations.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForSupport" {
    name               = "AWSServiceRoleForSupport"
    path               = "/aws-service-role/support.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "support.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForTrustedAdvisor" {
    name               = "AWSServiceRoleForTrustedAdvisor"
    path               = "/aws-service-role/trustedadvisor.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "trustedadvisor.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "AWSServiceRoleForVPCS2SVPN" {
    name               = "AWSServiceRoleForVPCS2SVPN"
    path               = "/aws-service-role/s2svpn.amazonaws.com/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "s2svpn.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-MIRROR-CT-Privatelink-CCFT-rLambdaRole-82BGT4M8529G" {
    name               = "CAT-MIRROR-CT-Privatelink-CCFT-rLambdaRole-82BGT4M8529G"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-MIRROR-CT-Privatelink-CCRP-rLambdaRole-LX19GC6UXWER" {
    name               = "CAT-MIRROR-CT-Privatelink-CCRP-rLambdaRole-LX19GC6UXWER"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-PrivateLink-CAUTH-rLambdaRole-KL2A91XSNV4N" {
    name               = "CAT-PrivateLink-CAUTH-rLambdaRole-KL2A91XSNV4N"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-PrivateLink-CCFT-rLambdaRole-1I2YCLSU5NY4J" {
    name               = "CAT-PrivateLink-CCFT-rLambdaRole-1I2YCLSU5NY4J"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-PrivateLink-CCRP-rLambdaRole-1AC1UNXA5AVTW" {
    name               = "CAT-PrivateLink-CCRP-rLambdaRole-1AC1UNXA5AVTW"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-PROD-Privatelink-CAUTH-rLambdaRole-1HEOHLLNTXQ6Q" {
    name               = "CAT-PROD-Privatelink-CAUTH-rLambdaRole-1HEOHLLNTXQ6Q"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-PROD-Privatelink-CCFT-rLambdaRole-RPI61UBZ5H69" {
    name               = "CAT-PROD-Privatelink-CCFT-rLambdaRole-RPI61UBZ5H69"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "CAT-PROD-Privatelink-CCRP-rLambdaRole-9XP4RTTB5T3C" {
    name               = "CAT-PROD-Privatelink-CCRP-rLambdaRole-9XP4RTTB5T3C"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role" "FinraAdmin" {
    name               = "FinraAdmin"
    path               = "/"
    assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

