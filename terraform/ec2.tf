resource "aws_instance" "i-03db670c49515b3e8" {
    ami                         = "ami-09f2114fecbe506e2"
    availability_zone           = "us-east-1a"
    ebs_optimized               = false
    instance_type               = "t2.micro"
    monitoring                  = false
    key_name                    = "Cat Key Pair"
    subnet_id                   = "subnet-04a3c7319981aea38"
    vpc_security_group_ids      = ["sg-0adb8ed88bb0246ca"]
    associate_public_ip_address = true
    private_ip                  = "10.1.69.152"
    source_dest_check           = true

    root_block_device {
        volume_type           = "gp2"
        volume_size           = 30
        delete_on_termination = true
    }

    tags {
    }
}

