resource "aws_subnet" "subnet-3f381601-subnet-3f381601" {
    vpc_id                  = "vpc-44d2e53e"
    cidr_block              = "172.31.64.0/20"
    availability_zone       = "us-east-1e"
    map_public_ip_on_launch = true

    tags {
    }
}

resource "aws_subnet" "subnet-04f63fd50f4cb6655-FinraNet-3" {
    vpc_id                  = "vpc-046a2dcfc1ed27038"
    cidr_block              = "10.1.32.0/20"
    availability_zone       = "us-east-1c"
    map_public_ip_on_launch = false

    tags {
        "Name" = "FinraNet-3"
    }
}

resource "aws_subnet" "subnet-5bf0eb3c-subnet-5bf0eb3c" {
    vpc_id                  = "vpc-44d2e53e"
    cidr_block              = "172.31.0.0/20"
    availability_zone       = "us-east-1a"
    map_public_ip_on_launch = true

    tags {
    }
}

resource "aws_subnet" "subnet-085d5f093626a0066-FinraNet-5" {
    vpc_id                  = "vpc-046a2dcfc1ed27038"
    cidr_block              = "10.1.80.0/20"
    availability_zone       = "us-east-1e"
    map_public_ip_on_launch = false

    tags {
        "Name" = "FinraNet-5"
    }
}

resource "aws_subnet" "subnet-52e40573-subnet-52e40573" {
    vpc_id                  = "vpc-44d2e53e"
    cidr_block              = "172.31.80.0/20"
    availability_zone       = "us-east-1b"
    map_public_ip_on_launch = true

    tags {
    }
}

resource "aws_subnet" "subnet-4dc26800-subnet-4dc26800" {
    vpc_id                  = "vpc-44d2e53e"
    cidr_block              = "172.31.16.0/20"
    availability_zone       = "us-east-1c"
    map_public_ip_on_launch = true

    tags {
    }
}

resource "aws_subnet" "subnet-04a3c7319981aea38-FinraNet-1" {
    vpc_id                  = "vpc-046a2dcfc1ed27038"
    cidr_block              = "10.1.64.0/20"
    availability_zone       = "us-east-1a"
    map_public_ip_on_launch = false

    tags {
        "Name" = "FinraNet-1"
    }
}

resource "aws_subnet" "subnet-04562c1270d80ca1e-FinraNet-6" {
    vpc_id                  = "vpc-046a2dcfc1ed27038"
    cidr_block              = "10.1.0.0/20"
    availability_zone       = "us-east-1f"
    map_public_ip_on_launch = false

    tags {
        "Name" = "FinraNet-6"
    }
}

resource "aws_subnet" "subnet-01d926c4a197736d8-FinraNet-2" {
    vpc_id                  = "vpc-046a2dcfc1ed27038"
    cidr_block              = "10.1.16.0/20"
    availability_zone       = "us-east-1b"
    map_public_ip_on_launch = false

    tags {
        "Name" = "FinraNet-2"
    }
}

resource "aws_subnet" "subnet-d171acdf-subnet-d171acdf" {
    vpc_id                  = "vpc-44d2e53e"
    cidr_block              = "172.31.48.0/20"
    availability_zone       = "us-east-1f"
    map_public_ip_on_launch = true

    tags {
    }
}

resource "aws_subnet" "subnet-debd5a81-subnet-debd5a81" {
    vpc_id                  = "vpc-44d2e53e"
    cidr_block              = "172.31.32.0/20"
    availability_zone       = "us-east-1d"
    map_public_ip_on_launch = true

    tags {
    }
}

resource "aws_subnet" "subnet-080db3c489dddde5b-FinraNet-4" {
    vpc_id                  = "vpc-046a2dcfc1ed27038"
    cidr_block              = "10.1.48.0/20"
    availability_zone       = "us-east-1d"
    map_public_ip_on_launch = false

    tags {
        "Name" = "FinraNet-4"
    }
}

