resource "aws_network_acl" "acl-247de359" {
    vpc_id     = "vpc-44d2e53e"
    subnet_ids = ["subnet-5bf0eb3c", "subnet-52e40573", "subnet-debd5a81", "subnet-4dc26800", "subnet-3f381601", "subnet-d171acdf"]

    ingress {
        from_port  = 0
        to_port    = 0
        rule_no    = 100
        action     = "allow"
        protocol   = "-1"
        cidr_block = "0.0.0.0/0"
    }

    egress {
        from_port  = 0
        to_port    = 0
        rule_no    = 100
        action     = "allow"
        protocol   = "-1"
        cidr_block = "0.0.0.0/0"
    }

    tags {
    }
}

resource "aws_network_acl" "acl-0a41d4ee487dfaa01" {
    vpc_id     = "vpc-046a2dcfc1ed27038"
    subnet_ids = ["subnet-04f63fd50f4cb6655", "subnet-080db3c489dddde5b", "subnet-04a3c7319981aea38", "subnet-04562c1270d80ca1e", "subnet-01d926c4a197736d8", "subnet-085d5f093626a0066"]

    ingress {
        from_port  = 0
        to_port    = 0
        rule_no    = 100
        action     = "allow"
        protocol   = "-1"
        cidr_block = "0.0.0.0/0"
    }

    egress {
        from_port  = 0
        to_port    = 0
        rule_no    = 100
        action     = "allow"
        protocol   = "-1"
        cidr_block = "0.0.0.0/0"
    }

    tags {
    }
}

