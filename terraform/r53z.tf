resource "aws_route53_zone" "-private" {
    name       = ""
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "ewslogin-ct-fip-catnms-com-private" {
    name       = "ewslogin-ct.fip.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "ews-ct-fip-catnms-com-private" {
    name       = "ews-ct.fip.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "reporterportal-pl-ct-catnms-com-private" {
    name       = "reporterportal-pl.ct.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "sftp-pl-ct-catnms-com-private" {
    name       = "sftp-pl.ct.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "cauth-catnms-com-private" {
    name       = "cauth.catnms.com"
    comment    = "CAT - cauth service (do not delete)"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "ccrp-catnms-com-private" {
    name       = "ccrp.catnms.com"
    comment    = "CAT - ccrp Service (do not delete)"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "ccft-catnms-com-private" {
    name       = "ccft.catnms.com"
    comment    = "CAT - ccft Service (do not delete)"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "sftp-pl-catnms-com-private" {
    name       = "sftp-pl.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "reporterportal-pl-catnms-com-private" {
    name       = "reporterportal-pl.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "ews-fip-catnms-com-private" {
    name       = "ews.fip.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

resource "aws_route53_zone" "ewslogin-fip-catnms-com-private" {
    name       = "ewslogin.fip.catnms.com"
    comment    = "Private Hosted Zone For FINRA CAT - do not delete"
    vpc_id     = "vpc-046a2dcfc1ed27038"
    vpc_region = "us-east-1"

    tags {
    }
}

